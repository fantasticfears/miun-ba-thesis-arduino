cmake_minimum_required(VERSION 2.8.4)

set(CMAKE_TOOLCHAIN_FILE ${CMAKE_SOURCE_DIR}/arduino-cmake/cmake/ArduinoToolchain.cmake)
set(PROJECT_NAME testr)
project(${PROJECT_NAME})
# set(ARDUINO_CPU 16MHzatmega328)

set(${CMAKE_PROJECT_NAME}_SKETCH test.ino)
set(${CMAKE_PROJECT_NAME}_BOARD uno)
set(${CMAKE_PROJECT_NAME}_PORT raspberrypi.local:/dev/ttyACM0)
set(${CMAKE_PROJECT_NAME}_SERIAL screen @SERIAL_PORT@)

generate_arduino_firmware(${CMAKE_PROJECT_NAME})

print_board_list()
print_programmer_list()

# add libraries to project
link_directories(${ARDUINO_SDK}/libraries)
