
#include <asip.h>       // the base class definitions
#include <asipIO.h>     // the core I/O class definition
#include <asipServos.h> // derived definitions for servo
#include <Servo.h> // needed for the servo service 


char const *sketchName = "SensibleThings";

//declare servo object(s) 
const byte NBR_SERVOS 
=1; // Could be more than one!
const byte servoPins[]    = {3}; // Pin to which servo is attached: change as appropriate
Servo myServos[NBR_SERVOS];  // create servo objects
asipCHECK_PINS(servoPins[NBR_SERVOS]);  // compiler will check if the number of pins is same as number of servos

asipServoClass asipServos(id_SERVO_SERVICE, NO_EVENT);
                 
// make a list of the created services
asipService services[] = { 
                                 &asipIO, // the core class for pin level I/O
                                 &asipServos
         };

void setup()
{
  Serial.begin(57600);
  asipIO.begin(); 
  // start the services
  asipServos.begin(NBR_SERVOS,servoPins,myServos);
  asip.begin(&Serial, asipServiceCount(services), services, sketchName); 
  asip.reserve(SERIAL_RX_PIN);  // reserve pins used by the serial port 
  asip.reserve(SERIAL_TX_PIN);  // these defines are in asip/boards.h 

  asip.sendPinModes(); // for debug
  asip.sendPortMap(); 
}

void loop() 
{
  asip.service();
}

